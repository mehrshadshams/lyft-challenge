import cv2
import os
import glob
import numpy as np
import argparse
import logging
from deeplab import Deeplabv3
from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Conv2DTranspose, Add, Lambda, ZeroPadding2D, Cropping2D, \
    Activation
from keras.initializers import RandomNormal
from keras.applications import VGG16
from keras import regularizers
from keras.models import Model
from keras import backend as K
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from app.icnet import model as icnet


def generator(X, y, batch_size=32, resize=None, training=False):
    """
    This is the generator that loads, extends and returns the images and their corresponding steering wheel
    angle for a given batch
    """
    num_samples = len(X)
    while 1:
        X, y = shuffle(X, y)
        for offset in range(0, num_samples, batch_size):
            end = offset + batch_size
            batch_X, batch_y = X[offset:end], y[offset:end]

            images = []
            segments = []
            for row in range(len(batch_X)):
                raw_file, seg_file = batch_X[row], batch_y[row]

                raw_file = os.path.join(os.getcwd(), raw_file)
                seg_file = os.path.join(os.getcwd(), seg_file)

                raw_image = cv2.imread(raw_file)
                raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)

                # if resize:
                #     raw_image = cv2.resize(raw_image, resize)

                seg_image = cv2.imread(seg_file)[:, :, -1]

                # if resize:
                #     seg_image = cv2.resize(seg_image, resize)

                # Only extend image in training
                # if training:
                #     image, angle = extend_image(image, angle)

                images.append(raw_image)
                segments.append(seg_image)

            images = np.array(images)
            segments = to_categorical(np.array(segments).reshape(batch_size, -1), num_classes=13)

            yield shuffle(images, segments)


def vgg16(input_shape=(600, 600, 3), num_classes=13):
    inp = Input(input_shape)

    def pad(x):
        return K.spatial_2d_padding(x, padding=((2, 2), (0, 0)))

    def preprocess(x):
        x /= 127.5
        x -= 1
        # mean = [0.485, 0.456, 0.406]
        # std = [0.229, 0.224, 0.225]
        # x[..., 0] -= mean[0]
        # x[..., 1] -= mean[1]
        # x[..., 2] -= mean[2]
        # if std is not None:
        #     x[..., 0] /= std[0]
        #     x[..., 1] /= std[1]
        #     x[..., 2] /= std[2]
        return x

    def conv_block(x, filter_size, kernel=(3, 3), activation='relu', num_conv=2, max_pool=True):
        # Block 1
        for i in range(num_conv):
            x = Conv2D(filter_size, kernel, activation=activation, padding='same')(x)

        if max_pool:
            x = MaxPooling2D(padding='same')(x)

        return x

    x = Lambda(preprocess)(inp)
    x = Lambda(pad)(x)
    pool1 = conv_block(x, 64)
    pool2 = conv_block(pool1, 128)
    pool3 = conv_block(pool2, filter_size=256, num_conv=3)
    # pool3 = Lambda(pad)(pool3)

    pool4 = conv_block(pool3, filter_size=512, num_conv=3)
    pool5 = conv_block(pool4, filter_size=512, num_conv=3)

    x = Conv2D(4096, (1, 1), padding='same')(pool5)
    x = Conv2D(4096, (1, 1), padding='same')(x)

    ##################

    # model = Model(inputs=inp, outputs=x)
    # print(model.summary())

    a = Conv2D(num_classes, (1, 1), kernel_initializer=RandomNormal(stddev=0.01), padding='same',
               kernel_regularizer=regularizers.l2(1e-3))(x)

    # upsample
    a = Conv2DTranspose(num_classes, 4, strides=(2, 2), padding='same',
                        kernel_initializer=RandomNormal(stddev=0.01),
                        kernel_regularizer=regularizers.l2(1e-3))(a)

    b = Conv2D(num_classes, (1, 1), kernel_initializer=RandomNormal(stddev=0.01), padding='same',
               kernel_regularizer=regularizers.l2(1e-3))(pool4)

    b = Add()([a, b])

    b = Conv2DTranspose(num_classes, 4, strides=(2, 2), padding='same',
                        kernel_initializer=RandomNormal(stddev=0.01),
                        kernel_regularizer=regularizers.l2(1e-3))(b)

    c = Conv2D(num_classes, (1, 1), kernel_initializer=RandomNormal(stddev=0.01), padding='same',
               kernel_regularizer=regularizers.l2(1e-3))(pool3)

    c = Add()([b, c])

    out = Conv2DTranspose(num_classes, (16, 16), strides=(8, 8), padding='same',
                          kernel_initializer=RandomNormal(stddev=0.01),
                          kernel_regularizer=regularizers.l2(1e-3))(c)

    out = Cropping2D(cropping=((4, 4), (0, 0)))(out)

    out = Activation(activation='softmax')(out)

    model = Model(inputs=inp, outputs=out)

    return model


def copy_weights(src, dst):
    source_convs = list(filter(lambda x: type(x) == Conv2D, src.layers))
    dst_layers = list(filter(lambda x: type(x) == Conv2D, dst.layers))
    for idx, conv in enumerate(source_convs):
        source_weights = conv.get_weights()
        dst_layers[idx].set_weights(source_weights)


def main(args):
    X = glob.glob('data/source/CameraRGB/*.png')
    y = glob.glob('data/source/CameraSeg/*.png')

    X, y = shuffle(X, y, random_state=1)

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.1, random_state=1)

    is_compiled = False
    if args.model == 'vgg16':
        vgg16_std = VGG16(classes=13, input_shape=(604, 800, 3), include_top=False)
        model = vgg16(input_shape=(600, 800, 3))
        copy_weights(vgg16_std, model)
    elif args.model == 'deeplab':
        model = Deeplabv3(classes=13)
    elif args.model == 'icnet':
        model, _ = icnet.build(13, 800, 600)
        is_compiled = True

    if not os.path.exists('models'):
        os.mkdir('./models')

    model_path = 'models/' + args.model
    if not os.path.exists(model_path):
        os.mkdir(model_path)

    model_path = os.path.join(os.getcwd(), model_path)

    if not is_compiled:
        optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

        model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['acc'])

    print(model.summary())

    train_generator = generator(X_train, y_train, resize=(512, 512), batch_size=args.batch_size)
    valid_generator = generator(X_val, y_val, resize=(512, 512), batch_size=args.batch_size)

    early_stop = EarlyStopping(monitor='loss', min_delta=0.001, patience=3, mode='min', verbose=1)
    checkpoint = ModelCheckpoint(model_path + '/weights.{epoch:02d}-{val_loss:.5f}.h5', monitor='val_loss', verbose=1,
                                 save_best_only=True, mode='min', period=1)
    logging = TensorBoard(log_dir='./logs/' + args.model)

    model.fit_generator(train_generator, steps_per_epoch=len(X_train) // args.batch_size,
                        validation_data=valid_generator, validation_steps=len(X_val) // args.batch_size,
                        epochs=args.epochs,
                        callbacks=[checkpoint, early_stop, logging])

    print('training finished')

    # model.load_weights('weights.10-0.10691.h5')

    # x = cv2.imread(X[150])
    # x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    #
    # img = np.expand_dims(x, axis=0)
    # y = model.predict(img)
    #
    # print(y.shape)
    # print(np.argmax(y))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='lyft challenge')

    parser.add_argument(
        "--model", choices=['vgg16', 'deeplab', 'icnet'], help="Model to use")
    parser.add_argument(
        "--epochs", metavar='N', type=int, default=10, help="Number of epochs")
    parser.add_argument(
        "--batch-size", metavar='N', type=int, default=8, help="batch size")

    args = parser.parse_args()

    main(args)
