#!/usr/bin/env bash

rm -rf logs/*

nohup tensorboard --logdir=./logs &

nohup python train.py &